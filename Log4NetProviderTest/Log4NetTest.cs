﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Serilog;
using System.IO;
using System.Linq;

namespace Log4NetProviderTest
{
    [TestClass]
    public class Log4NetTest
    {
        //see https://blog.rsuter.com/logging-with-ilogger-recommendations-and-best-practices/ for best practices

        private ILoggerFactory loggerFactory = null;

        [TestInitialize]
        public void Setup()
        {
            loggerFactory = new LoggerFactory();//creates logger factory
        }

        private void DoLog()
        {
            Microsoft.Extensions.Logging.ILogger logger = loggerFactory.CreateLogger<Log4NetTest>();
            logger.LogInformation("This is a logging test");
        }

        [TestMethod]
        public void TestConsole()
        {

            //logging via console.
            var configureNamedOptions = new ConfigureNamedOptions<ConsoleLoggerOptions>("", null);
            var optionsFactory = new OptionsFactory<ConsoleLoggerOptions>(new[] { configureNamedOptions }, Enumerable.Empty<IPostConfigureOptions<ConsoleLoggerOptions>>());
            var optionsMonitor = new OptionsMonitor<ConsoleLoggerOptions>(optionsFactory, Enumerable.Empty<IOptionsChangeTokenSource<ConsoleLoggerOptions>>(), new OptionsCache<ConsoleLoggerOptions>());

            ILoggerProvider provider = new ConsoleLoggerProvider(optionsMonitor);

            loggerFactory.AddProvider(provider);//add console provider to logger Factory

            DoLog();//log always the same phrase
        }

        [TestMethod]
        public void TestSerilog()
        {
            //serilog is considered everywhere the best implmentation for logging
            //It integrates with Microsoft.Extensions.Logging
            //WARNING!!!!! I had to downgrade Microsoft.Extensions.Logging to 2.0 for compatibility Issues...
            //https://github.com/serilog/serilog-extensions-logging/issues/158
            //so bad...
            var serilogLogger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File(@"log\log-.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();

            ILoggerProvider provider = new Serilog.Extensions.Logging.SerilogLoggerProvider(serilogLogger, true);
            loggerFactory.AddProvider(provider);

            DoLog();//log always the same phrase
        }

        [TestMethod]
        public void TestLog4Net()
        {
            //see https://www.michalbialecki.com/2018/12/21/adding-a-log4net-provider-in-net-core-console-app/

            //log4net is not integrated with Microsoft.Extensions.Logging.
            //There are third party integrations, but not supported from the original project 
            //framework is outdated, supporting only netStandard 1.3/NetCore 1.0 https://logging.apache.org/log4net/release/framework-support.html
            //so I decidede to follow instructions and create My Own implementation
            FileInfo info = new FileInfo("log4net.config");
            log4net.Config.XmlConfigurator.Configure(info);

            ILoggerProvider provider = new Log4NetProvider.Log4NetProvider(false);
            loggerFactory.AddProvider(provider);

            DoLog();//log always the same phrase
        }

        [TestMethod]
        public void TestNLog()
        {
            //NLog is another alternative for Logging, similar to Log4Net, but it's newer and supports .NetCore (https://nlog-project.org/)
            //WARNING!!! seems that also NLog has the same Problem as SeriLog with dependencies
            //needs version 2.1.0.0
            NLog.LogManager.Configuration = new NLog.Config.XmlLoggingConfiguration("nlog.config");

            ILoggerProvider provider = new NLog.Extensions.Logging.NLogLoggerProvider();
            loggerFactory.AddProvider(provider);

            DoLog();//log always the same phrase
        }
    }
}
