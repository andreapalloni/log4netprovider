# README #

This project is a sample on how to create a log4net provider for Microsoft.Extensions.Logging and .Net Framework 4.8

### What is this repository for? ###

The new Microsoft.Extensions.Logging is the implementation for logging in .Net Core  
In .Net Framework there is not a common implementation for logging. Usually log4net is the library used for logging.  
But what happens if I want to adapt a Net Framework 4.8 project to be .Net Core compatible while using log4net as the logging implementation?  
log4net has not a ready to use provider, like NLog & Serilog, so I need to write a provider implementation.

### Who do I talk to? ###

* Follow me on https://andreapalloni.blogspot.com
